const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000; // works when hosting
const mongoose = require('mongoose');
const courseRoutes = require('./routes/courseRoutes')

mongoose.connect('mongodb+srv://adminDB:FhXqMxMv8TqbV7@batch139.ndmwa.mongodb.net/course-booking', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.use("/api/courses", courseRoutes);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Failed to connect to database'));
db.once('open', () => console.log(`Successfully connected to database`));

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));
