const express = require("express");
const router = express.Router();
const courseControllers = require("./../controllers/courseControllers")

router.post("/create-course", (req, res) => {
    courseControllers.createCourse(req.body).then(result => res.send(result));
})

module.exports = router

router.get("/get-courses", (req, res) => {
    courseControllers.getCourses().then(result => res.send(result));
})