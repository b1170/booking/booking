const Course = require("../models/Course");

module.exports.createCourse = (reqBody) => {

    let newCourse = new Course({
        courseName: reqBody.courseName,
        description: reqBody.description,
        price: reqBody.price,
    });

    return newCourse.save().then((res, err)=> {
        if(err) {
            return false
        } else {
            return true
        }
    })
}

module.exports.getCourses = (reqBody) => {
    return Course.find().then((result) => {return result});
}

module.exports.activeCourses = (reqBody) => {
    return Course.find({isActive:true}).then((result) => {return result});
}
